### Particle41 DevOps Challenge -- Senior Level

This is an senior level challange to test your level of familiratiy with development and operation tools and concepts.

---

## The challenge

The DevOps team is getting a lot of requests from dev teams for  creating new repositiories for their microservices. So DevOps team decide to automate the process for creating the repository.

DevOps team decide to create a main repository which conists of a ```repo_list``` file in which whenever they receive request for new repo they will update the ```repo_list``` file and the repo will be created automatically.


Note:
* You can choose any version control software.
* you can use any cloud provider of your choice
* any software/servers should deployed using terraform/cloudformation/aws cdk/ARM template etc.



---
 

## Criteria

*** 
* This repo consists of repo_list file (can contain any number of repositories list)
* your solution create all the repositories present in ```repo_list``` file.

Your task will considered successful if repo creation 
Other criteria for evaluation will be:
***
    1. Code quality and style: your code must be easy for others to read, and properly documented when relevant
    2. A good README.md file with clear instruction on how to automate the process.

---

